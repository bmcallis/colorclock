const getTimeText = (date, timeZone) => date.toLocaleTimeString('en-us', { timeZone, timeZoneName: 'short', hour: '2-digit', minute: '2-digit' });
const getBackgroundColor = date => `hsl(${Math.round(date.getSeconds() * 6)}, 60%, 30%, 1)`;
const getTextColor = date => `hsl(${Math.round(date.getSeconds() * 6) - 180}, 60%, 30%, 1)`;

const changeBgColor = () => {
  const now = new Date();
  document.body.style.setProperty('--bg-color', getBackgroundColor(now));
  // document.body.style.setProperty('--text-color', getTextColor(now));
  document.getElementById('omaha').innerText = getTimeText(now, 'America/Chicago');
  document.getElementById('newyork').innerText = getTimeText(now, 'America/New_York');
  document.getElementById('vegas').innerText = getTimeText(now, 'America/Los_Angeles');
};

document.onreadystatechange = () => {
  changeBgColor();
  setInterval(changeBgColor, 10000);
};
